var datePicker;
	var dateNmaeType = ['일', '월', '화', '수', '목', '금', '토'];
	
	//datepicker 초기설정
	var dateInit = function(){
		$.datepicker.setDefaults({
			dateFormat: 'yy-mm-dd',
			prevText: '이전 달',
			nextText: '다음 달',
			monthNames: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			monthNamesShort: ['1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월'],
			dayNames: dateNmaeType,
			dayNamesShort: dateNmaeType,
			dayNamesMin: dateNmaeType,
			showMonthAfterYear: true,
			changeYear: false,
			yearSuffix: '년',
		});
	}

	var dateRange = function(){
		var dateRangSet = $('.cs-datepicker-set');
		dateRangSet.each(function(){
			var row = $(this);
			var from = row.find('.js__date-from');
			var to = row.find('.js__date-to');
			from.datepicker({
			}).on( "change", function() {
				to.datepicker( "option", "minDate", getDateRang( this ) );
			});
			to.datepicker({
			}).on( "change", function() {
				from.datepicker( "option", "maxDate", getDateRang( this ) );
			});

		});
	}

	var getDateRang  = function( element ) {
		var date;
		try {
			date = $.datepicker.parseDate( "yy.mm.dd", element.value );
		} catch( error ) {
			date = null;
		}

		return date;
	}
	setDatePicker = function(){
		//datepicker 한글설정
		dateInit();

		$('.js__datepicker').datepicker({
			changeMonth: false,
			changeYear: false,
			showOn: 'button',
        	buttonImageOnly: false,
		});

		dateRange();
	}
	getDate = function(date){
		if(!date) date = new Date();
		var y = date.getFullYear();
				m = date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : date.getMonth() + 1;
				d = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();

				return y + '-' + m + '-' + d;
	}
    setDatePicker();

	$('.js_tab button').on('click', function(){
		var tid = $(this).data('tab-id');

		if($(this).is('.active')){

		} else {
			$('[data-id="'+tid+'"]').addClass('_show')
			.siblings('.tab-panel').removeClass('_show');
			$(this).addClass('active')
			.siblings('button').removeClass('active');
		}
	})

	$('.btn-urmenu').on('click' , function(){
		$('.user-tooltip').show()
	})
	$('.user-tooltip .close').on('click' , function(){
		$('.user-tooltip').hide()
	})

	$('.toggle-item').on('click' , function(){

		var _this = $(this),
			_pa = _this.parent('li');

		if(_pa.is('.active')){
			_pa.removeClass('active');
			_pa.find('.slide-menu').stop().slideUp(300);
		} else {
			_pa.find('.slide-menu').stop().slideDown(300);
			_pa.siblings('.toggle-li').removeClass('active')
			_pa.siblings('.toggle-li').find('.slide-menu').stop().slideUp(300);
			_pa.addClass('active');
		}

		return false;
	})